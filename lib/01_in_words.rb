class Fixnum

  def in_words
    result = []
    temp_digits = self.to_s.split("")
    #if temp_digits[-2] == "1"
      #temp_digits[-2] += temp_digits[-1]
    #  temp_digits.delete_at(-1)
  #  end
    digits = temp_digits.map { |digit| Integer(digit) }
    return "zero" if digits.join.to_i == 0
    if digits.length % 3 != 0
      temp_length = digits.length
      temp_combo = digits.shift(digits.length % 3)
      result << hundreds(temp_combo.join.to_i)
      result << prefix(temp_length)
    end
    until digits == []
      temp_length = digits.length
      temp_trio = digits.shift(3)
      result << hundreds(temp_trio.join.to_i)
      result << prefix(temp_length) if temp_trio.join.to_i != 0
    end
    result.delete("")
    result.join(" ")
  end

  private

  def specials(num, multi_digit=true)
    case num
    when 0
      return "zero" if !multi_digit
      return "" if multi_digit
    when 1
      return "one"
    when 2
      return "two"
    when 3
      return "three"
    when 4
      return "four"
    when 5
      return "five"
    when 6
      return "six"
    when 7
      return "seven"
    when 8
      return "eight"
    when 9
      return "nine"
    when 10
      return "ten"
    when 11
      return "eleven"
    else
      return "twelve"
    end
  end

  def teens(num)
    case num
    when 13
      result = "thir"
    when 14
      result = "four"
    when 15
      result = "fif"
    when 16
      result = "six"
    when 17
      result = "seven"
    when 18
      result = "eigh"
    else
      result = "nine"
    end
    result + "teen"
  end

  def tens(num)
    final_result = []
    if num < 13
      return specials(num, false)
    elsif num < 20
      return teens(num)
    else
      case num / 10
      when 2
        result = "twen"
      when 3
        result = "thir"
      when 4
        result = "for"
      when 5
        result = "fif"
      when 6
        result = "six"
      when 7
        result = "seven"
      when 8
        result = "eigh"
      when 9
        result = "nine"
      end
      result += "ty"
      ones_digit = specials(num % 10)
      final_result << result
      if ones_digit != ""
        final_result << ones_digit
      end
      final_result.join(" ")
    end
  end

  def hundreds(num)
    result = []
    hundred_digit = specials(num / 100)
    if hundred_digit != ""
      result << hundred_digit
      result << "hundred"
    end
    result << tens(num % 100)
    result.pop if result.last == "zero"
    result.join(" ")
  end

  def prefix(length)
    if length < 7 && length > 3
      return "thousand"
    elsif length < 10 && length > 6
      return "million"
    elsif length < 13 && length > 9
      return "billion"
    elsif length < 16 && length > 12
      return "trillion"
    else
      return ""
    end
  end

end
